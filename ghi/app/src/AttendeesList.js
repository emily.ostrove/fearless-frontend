import React, { useState } from "react";

function AttendeesList(props) {
    // Declare a new state variable, which we'll call "attendee"
    const [attendee, setAttendee] = useState(0);
    return (
        <table className="table table-striped">    
            <thead>
            <tr>
                <th>Name</th>
                <th>Conference</th>
            </tr>
            </thead>
            <tbody>
            {props.attendees.map(attendee => {
                return (
                <tr key={attendee.href}>
                    <td>{ attendee.name }</td>
                    <td>{ attendee.conference }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default AttendeesList;