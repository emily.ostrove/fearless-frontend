window.addEventListener('DOMContentLoaded', async () => {
    // Get the cookie out of the cookie store
    const payloadCookie = await cookieStore.get('jwt_access_payload');
    console.log(payloadCookie);
    if (payloadCookie) {
    // The cookie value is a JSON-formatted string, so parse it
        const encodedPayload = JSON.parse(payloadCookie.value);
        // Convert the encoded payload from base64 to normal string
        const decodedPayload = atob(encodedPayload)
        // The payload is a JSON-formatted string, so parse it
        const payload = JSON.parse(decodedPayload);

        // Print the payload
        console.log("Payload: ", payload);

        // Check if "events.add_conference" is in the permissions.
        if (payload.user.perms[28] === "events.add_conference") {
        // If it is, remove 'd-none' from the link
            console.log(payload.user.perms[28]);
            console.log(payload.user.perms);
            const addConference = document.getElementById('new-conference');
            addConference.classList.remove("d-none");
        }
        // Check if "events.add_location" is in the permissions.
        if (payload.user.perms[37] === "events.add_location") {
        // If it is, remove 'd-none' from the link
            const addLocation = document.getElementById('new-location');
            addLocation.classList.remove("d-none");
        }
}
});