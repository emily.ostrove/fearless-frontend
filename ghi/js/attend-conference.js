window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      };
      // add the 'd-none' class to the loading icon
      const loadingIcon = document.getElementById('loading-conference-spinner');
      loadingIcon.classList.add("d-none");
      //remove the 'd-none' class from the select tag
      selectTag.classList.remove("d-none");
    }
    // fetch data from the attendees
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log("proof json happens: ", json);
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log("check fetchConfig: ", fetchConfig)
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            // remove d-none from success alert
            const successAlert = document.getElementById('success-message');
            successAlert.classList.remove("d-none");
            // add d-none to the form
            formTag.classList.add("d-none");
        }
    });
  });