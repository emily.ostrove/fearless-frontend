window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
        // create the location dropdown
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            //console.log("these are the locations: ", data.locations)
            const selectSecondTag = document.getElementById('location');
            for (const location of data.locations) {
                const option = document.createElement('option');
                option.innerHTML = location.name;
                option.value = location.id;
                //console.log(location);
                selectSecondTag.appendChild(option);
            };
        }
        // fetch data from the Create button when someone clicks on it
        const formTag = document.getElementById('create-conference-form');
        console.log("what is priting ", formTag)
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);
            // code to send data to the server
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "POST",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
        const responseConference = await fetch(conferenceUrl, fetchConfig);
        if (responseConference.ok) {
            formTag.reset();
            const newConference = await responseConference.json();
            console.log(newConference);
        }
        });
    } catch (error) {
        console.error(error)
    } 
  });