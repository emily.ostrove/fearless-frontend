window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const selectTag = document.getElementById('conference');
    console.log(selectTag.item);

    // testing out here, will delete later
    const formTag = document.getElementById('create-presentation-form');
    console.log("formTag stuff ", formTag);
    //console.log(selectTag.option[selectTag.selectedIndex].value);
    try {
        // create the location dropdown
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            //console.log("find the conferences: ", data)
            const selectTag = document.getElementById('conference');
            console.log(selectTag);
            for (const conference of data.conferences) {
                const option = document.createElement('option');
                option.innerHTML = conference.name;
                // this produces the id next to conference in the href
                option.value = conference.href;
                selectTag.appendChild(option);
            };
        }
        // fetch data from the Create button when it's clicked
        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            //console.log(json)
            // code sending data to the server
            // first get ID of conference from select tag
            console.log("what is this tag ", selectTag.value)
            const presentationUrl = 'http://localhost:8000' + selectTag.value + 'presentations/';
            const fetchConfig = {
                method: "POST",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
            }
        });
    } catch (error) {
        console.error(error)
    } 
  });