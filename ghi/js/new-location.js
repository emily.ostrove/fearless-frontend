// add an event listener for where teh DOM loads
// declare a variable that will hold the URL for the API we created
// fetch the URL
// if response okay, get date with JSON method

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/states/';
  try {
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        console.log(data);
        const selectTag = document.getElementById('state');
        for (const state of data.states) {
            const option = document.createElement('option');
            option.innerHTML = state.name;
            option.value = state.abbreviation;
            selectTag.appendChild(option);
      };
    }
    // fetch data from the Create button when someone clicks submit
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log("proof json happens: ", json);
        // code to send data to the server
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const responseLocation = await fetch(locationUrl, fetchConfig);
        if (responseLocation.ok) {
            // don't need rest, which resets form to origianl state. We want to see if something happens
            formTag.reset();
            const newLocation = await responseLocation.json();
            //console.log(newLocation);
        }
    });

  } catch (error) {
      console.error(error);
  }
  
});

async function createStateDropdown() {
    //const stateData = await fetchJSON('http://localhost:8000/api/states/')
    //const states = stateData.states
    const selectTag = document.getElementById('state');
    for (const state of states) {
        const option = document.createElement('option');
        option.innerHTML = state.name;
        option.value = state.abbreviation;
        selectTag.appendChild(option)
    };
}

